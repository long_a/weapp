"use strict";

var _core = _interopRequireDefault(require('./../vendor.js')(0));

var _http = _interopRequireDefault(require('./../service/http.js'));

var _config = _interopRequireDefault(require('./../service/config.js'));

var _store = _interopRequireDefault(require('./../store/index.js'));

var _x = require('./../vendor.js')(4);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

_core["default"].page({
  store: _store["default"],
  data: {
    info: Object,
    timeContent: null,
    name: '',
    phone: '',
    smscode: '',
    list_t: '',
    commentsDetail: null,
    show: false,
    time_list: [],
    currentIndex: 0,
    selcetindex: Number
  },
  computed: _objectSpread({}, (0, _x.mapState)(['userInfo'])),
  methods: {
    onClose: function onClose() {
      this.show = false;
    },
    popupShow: function popupShow() {
      this.selcetindex = this.currentIndex;
      this.show = true;
    },
    selectTime: function selectTime() {
      var _this2 = this;

      _http["default"].request(_config["default"].url.selection_number, {
        group_but: this.info.id,
        datetime: this.commentsDetail.ymd,
        list_t: this.list_t
      }).then(function (res) {
        console.log(res);
        _this2.time_list = res.data;
        _this2.timeContent = _this2.time_list[0];
      });
    },
    define: function define() {
      this.show = false;
      this.currentIndex = this.selcetindex;
      this.timeContent = this.time_list[this.selcetindex];
    },
    formatDay: function formatDay(dayInfo, time, type) {
      var ymd, week, times;
      ymd = dayInfo.ymd.split('-');
      ymd = "".concat(ymd[0], "\u5E74").concat(ymd[1], "\u6708").concat(ymd[2], "\u53F7");
      var weekDay = ["星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"];
      week = weekDay[+dayInfo.week];
      times = time.split(' ')[1].split(':');
      times.pop();

      if (!type) {
        times = "".concat(times[0], "\u65F6").concat(times[1], "\u5206");
        return ymd + week + times;
      } else {
        return "".concat(dayInfo.ymd, "/").concat(week, " ").concat(this.list_t).concat(times[0], ":").concat(times[1]);
      }
    },
    getPay: function getPay(key, type) {
      var _this = this;

      var params, url;

      if (type) {
        url = _config["default"].url.deposit_identification_mz;
        params = {
          order_sn: key
        };
      } else {
        url = _config["default"].url.deposit;
        params = {
          money: key
        };
      }

      return new Promise(function (resolve, reject) {
        _http["default"].request(url, params).then(function (res) {
          wx.requestPayment({
            timeStamp: res.data.timeStamp,
            // 时间戳
            nonceStr: res.data.nonceStr,
            // 随机字符串
            "package": res.data["package"],
            // 统一下单接口返回的 prepay_id 参数值
            signType: res.data.signType,
            paySign: res.data.paySign,
            // 签名
            success: function success(res) {
              return resolve(res);
            },
            fail: function fail(err) {
              console.log('err', err);
              return reject(err);
            }
          });
        });
      });
    },
    homeIdentification: function homeIdentification() {
      var _this = this;

      var params = {
        id: _this.timeContent.id,
        smscode: _this.smscode,
        name: _this.name,
        mobile: _this.phone
      };

      _http["default"].request(_config["default"].url.homeIdentification, params).then(function (res) {
        var info = {
          type: 'doctor',
          mode: '到店就诊',
          day: "".concat(_this.formatDay(_this.commentsDetail, _this.timeContent.day_time, 1))
        };

        if (res.data.status == 103) {
          wx.showModal({
            title: '提示',
            content: '您还未缴纳保证金，请先充值',
            confirmText: '充值',
            confirmColor: '#28B9D1',
            success: function success(res) {
              if (res.confirm) {
                _this.getPay(0.01).then(function () {
                  wx.showToast({
                    title: '充值成功',
                    icon: 'success',
                    duration: 2000,
                    mask: true,
                    success: function success() {
                      setTimeout(function () {
                        wx.navigateBack(-1);
                      }, 2000);
                    }
                  });
                });
              }
            }
          });
        } else if (res.data.status == 101) {
          var order_sn = res.data.data.order_sn;

          _this.getPay(order_sn, 'order').then(function () {
            _this.$app.$options.globalData.successInfo = info;
            wx.redirectTo({
              url: '/pages/orderSuccess'
            });
          });
        } else {
          _this.$app.$options.globalData.successInfo = info;
          wx.redirectTo({
            url: '/pages/orderSuccess'
          });
        }
      })["catch"](function (err) {
        wx.showToast({
          title: '预约失败',
          icon: 'none',
          duration: 2000,
          mask: true,
          success: function success() {
            setTimeout(function () {
              wx.navigateBack(-1);
            }, 2000);
          }
        });
      });
    },
    submit: function submit() {
      var _this = this;

      console.log(_this.formatDay(_this.commentsDetail, _this.timeContent.day_time, 1));
      if (!this.name && !this.phone) return;
      console.log(this.name);
      console.log(this.phone);
      wx.showModal({
        title: '提示',
        content: "\u60A8\u786E\u5B9A\u8981\u9884\u7EA6:".concat(this.formatDay(this.commentsDetail, this.timeContent.day_time, 0), "-").concat(this.info.name, "\u7684\u95E8\u8BCA\u5417\uFF1F"),
        confirmColor: '#28B9D1',
        success: function success(res) {
          if (res.confirm) {
            _this.homeIdentification();
          }
        }
      });
    }
  },
  onLoad: function onLoad(option) {
    if (option.type == 0) this.list_t = '上午';
    if (option.type == 1) this.list_t = '下午';
    this.info = this.$app.$options.globalData.commentsDetail;
    this.commentsDetail = this.$app.$options.globalData.commentsDetail.comments[option.index];
    this.selectTime();
  },
  onShow: function onShow() {
    var baseInfo = this.$app.$options.globalData.info;

    if (baseInfo) {
      this.name = baseInfo.name;
      this.phone = baseInfo.phone;
      this.smscode = baseInfo.smscode;
    }
  }
}, {info: {"components":{"van-button":{"path":"./../components/lib/button/index"},"van-cell":{"path":"./../components/lib/cell/index"},"van-popup":{"path":"./../components/lib/popup/index"},"van-icon":{"path":"./../components/lib/icon/index"},"card":{"path":"./../components/card"}},"on":{"14-0":["tap"],"14-1":["tap"],"14-2":["close"],"14-4":["tap"],"14-5":["tap"]}}, handlers: {'14-0': {"tap": function proxy () {
    var $event = arguments[arguments.length - 1];
    var _vm=this;
      return (function () {
        _vm.popupShow($event);
      })();
    
  }},'14-1': {"tap": function proxy () {
    var $event = arguments[arguments.length - 1];
    var _vm=this;
      return (function () {
        _vm.submit($event);
      })();
    
  }},'14-2': {"close": function proxy () {
    var $event = arguments[arguments.length - 1];
    var _vm=this;
      return (function () {
        _vm.onClose($event);
      })();
    
  }},'14-3': {"tap": function proxy (index) {
    
    var _vm=this;
      return (function () {
        _vm.selcetindex = index;
      })();
    
  }},'14-4': {"tap": function proxy () {
    
    var _vm=this;
      return (function () {
        _vm.show = false;
      })();
    
  }},'14-5': {"tap": function proxy () {
    var $event = arguments[arguments.length - 1];
    var _vm=this;
      return (function () {
        _vm.define($event);
      })();
    
  }}}, models: {}, refs: undefined });
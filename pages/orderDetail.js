"use strict";

var _core = _interopRequireDefault(require('./../vendor.js')(0));

var _http = _interopRequireDefault(require('./../service/http.js'));

var _config = _interopRequireDefault(require('./../service/config.js'));

var _store = _interopRequireDefault(require('./../store/index.js'));

var _x = require('./../vendor.js')(4);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

_core["default"].page({
  store: _store["default"],
  data: {
    isOrder: true,
    orderDetail: null,
    tabIndex: null,
    index: null
  },
  computed: _objectSpread({}, (0, _x.mapState)(['orderList', 'doorList'])),
  methods: _objectSpread({}, (0, _x.mapMutations)(['getDoorList', 'getOrderList']), {
    previewImage: function previewImage(url) {
      wx.previewImage({
        current: url,
        urls: this.orderDetail.enclosure
      });
    },
    submit: function submit() {
      var _this = this;

      var url;
      wx.showModal({
        title: '提示',
        content: '是否确认取消预约？',
        confirmColor: '#28B9D1',
        success: function success(res) {
          if (res.confirm) {
            if (_this.isOrder) {
              url = _config["default"].url.emplacement_clear;
            } else {
              url = _config["default"].url.identification_clear;
            }

            _http["default"].request(url, {
              id: _this.orderDetail.id
            }).then(function (res) {
              if (_this.tabIndex == 0) {
                _this.orderList[_this.index].stu = 3;

                _this.getOrderList(_this.orderList);
              } else {
                _this.doorList[_this.index].status = 3;

                _this.getDoorList(_this.doorList);
              }

              wx.showToast({
                title: res.data,
                icon: 'success',
                duration: 2000,
                mask: true,
                success: function success() {
                  setTimeout(function () {
                    wx.navigateBack(-1);
                  }, 2000);
                }
              });
            });
          }
        }
      });
    }
  }),
  onLoad: function onLoad(option) {
    this.index = option.index;
    this.tabIndex = option.tabIndex;
    console.log('this.$app.$options.globalData.successInfo', this.$app.$options.globalData.successInfo);

    if (option.tabIndex == 0) {
      this.$app.$options.globalData.successInfo = {
        type: 'doctor'
      };
      this.orderDetail = this.orderList[option.index];
      this.isOrder = true;
    } else {
      this.$app.$options.globalData.successInfo = {
        type: 'door'
      };
      this.orderDetail = this.doorList[option.index];
      this.isOrder = false;
    }
  }
}, {info: {"components":{"van-button":{"path":"./../components/lib/button/index"},"van-cell":{"path":"./../components/lib/cell/index"},"van-cell-group":{"path":"./../components/lib/cell-group/index"}},"on":{}}, handlers: {'19-0': {"tap": function proxy (src) {
    
    var _vm=this;
      return (function () {
        _vm.previewImage(src);
      })();
    
  }},'19-1': {"tap": function proxy () {
    var $event = arguments[arguments.length - 1];
    var _vm=this;
      return (function () {
        _vm.submit($event);
      })();
    
  }}}, models: {}, refs: undefined });
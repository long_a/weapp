"use strict";

var _core = _interopRequireDefault(require('./../vendor.js')(0));

var _http = _interopRequireDefault(require('./../service/http.js'));

var _config = _interopRequireDefault(require('./../service/config.js'));

var _store = _interopRequireDefault(require('./../store/index.js'));

var _x = require('./../vendor.js')(4);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

_core["default"].page({
  store: _store["default"],
  data: {
    tel: '400-888-8888',
    startDay: '',
    endDay: '',
    addresVal: [],
    phone: '',
    // 手机号
    day_time: '',
    // 上门日期
    day_times: '',
    // 上门时间
    addres: '',
    // 上门地址
    address: '',
    // 上门详细地址
    name: '',
    // 就诊人姓名
    sex: '男',
    // 就诊人性别
    age: '',
    // 就诊人年龄
    near: '',
    // 就诊人近况
    nears: '',
    // 就诊人病史
    imgList: [],
    remark: '',
    // 备注
    checked: true,
    Imglength: 0
  },
  computed: _objectSpread({
    addres: function addres() {
      return this.addresVal.join('');
    },
    enclosure: function enclosure() {
      // 附件
      return this.imgList.join(',');
    }
  }, (0, _x.mapState)(['doorDetail', 'userInfo', 'system_info'])),
  methods: {
    getDay: function getDay(days) {
      var date = new Date();
      if (days) date.setDate(date.getDate() + days);
      var year = date.getFullYear();
      var month = date.getMonth() + 1;
      var day = date.getDate();
      var nowData = year + "-" + month + "-" + day;
      return nowData;
    },
    toTel: function toTel() {
      wx.makePhoneCall({
        phoneNumber: this.system_info.contact
      });
    },
    delImg: function delImg(i) {
      this.imgList.splice(i, 1);
    },
    previewImage: function previewImage(url) {
      wx.previewImage({
        current: url,
        urls: this.imgList
      });
    },
    explain: function explain() {
      // 说明
      wx.showModal({
        title: '提示',
        showCancel: false,
        content: '这里的费用仅为单项上门收取的费用，如果上门服务过程中发生其他额外费用将由上门医生或护士根据收费标准向您收取。',
        confirmText: '已知',
        confirmColor: '#28B9D1',
        success: function success(res) {
          if (res.confirm) {}
        }
      });
    },
    boxChange: function boxChange() {
      this.checked = !this.checked;
    },
    radioChange: function radioChange() {
      console.log(this.radioVal);
    },
    getPay: function getPay(key, type) {
      var _this = this;

      console.log('支付');
      var params, url;

      if (type) {
        url = _config["default"].url.deposit_identification;
        params = {
          order_sn: key
        };
      } else {
        url = _config["default"].url.deposit;
        params = {
          money: key
        };
      }

      return new Promise(function (resolve, reject) {
        _http["default"].request(url, params).then(function (res) {
          wx.requestPayment({
            timeStamp: res.data.timeStamp,
            // 时间戳
            nonceStr: res.data.nonceStr,
            // 随机字符串
            "package": res.data["package"],
            // 统一下单接口返回的 prepay_id 参数值
            signType: res.data.signType,
            paySign: res.data.paySign,
            // 签名
            success: function success(res) {
              return resolve(res);
            },
            fail: function fail(err) {
              console.log('err', err);
              return reject(err);
            }
          });
        });
      });
    },
    verification: function verification() {
      if (!this.phone) {
        wx.showToast({
          title: '请输入手机号',
          icon: 'none',
          mask: true
        });
        return false;
      }

      if (!/^1[0-9][0-9]{9}$/.test(this.phone)) {
        wx.showToast({
          title: '请输入正确的手机号',
          icon: 'none',
          mask: true
        });
        return false;
      }

      if (!this.day_time) {
        wx.showToast({
          title: '请选择上门日期',
          icon: 'none',
          mask: true
        });
        return false;
      }

      if (!this.day_times) {
        wx.showToast({
          title: '请选择上门时间',
          icon: 'none',
          mask: true
        });
        return false;
      }

      if (!this.addres) {
        wx.showToast({
          title: '请选择上门地址',
          icon: 'none',
          mask: true
        });
        return false;
      }

      if (!this.address) {
        wx.showToast({
          title: '请输入上门详细地址',
          icon: 'none',
          mask: true
        });
        return false;
      }

      if (!this.name) {
        wx.showToast({
          title: '请输入姓名',
          icon: 'none',
          mask: true
        });
        return false;
      }

      if (!this.sex) {
        wx.showToast({
          title: '请选择性别',
          icon: 'none',
          mask: true
        });
        return false;
      }

      if (!this.age) {
        wx.showToast({
          title: '请输入年龄',
          icon: 'none',
          mask: true
        });
        return false;
      }

      if (!this.checked) {
        wx.showToast({
          title: '请勾选服务协议',
          icon: 'none',
          mask: true
        });
        return false;
      }

      return true;
    },
    formatDay: function formatDay(day_time, day_times, type) {
      console.log(day_time);
      var ymd, week, times;
      var weekDay = ["星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"];
      var myDate = type ? day_time : day_time.split(' ');
      week = weekDay[new Date(type ? myDate : myDate[0]).getDay()];
      var date = type ? day_time : myDate[0];
      times = type ? day_times : myDate[1].split(':').join(':');
      var timeValue = "" + (times.split(':')[0] >= 12 ? "下午" : "上午");
      return "".concat(day_time, "/").concat(week, " ").concat(timeValue).concat(times);
    },
    identification: function identification() {
      var _this = this;

      var params = {
        group_but: _this.doorDetail.id,
        phone: _this.phone,
        day_time: _this.day_time,
        day_times: _this.day_times,
        addres: _this.addres,
        address: _this.address,
        name: _this.name,
        sex: _this.sex,
        age: _this.age,
        near: _this.near,
        nears: _this.nears,
        enclosure: _this.enclosure,
        remark: _this.remark
      };

      _http["default"].request(_config["default"].url.identification, params).then(function (res) {
        var info = {
          type: 'door',
          mode: '上门服务',
          title: _this.doorDetail.title,
          day: _this.formatDay(_this.day_time, _this.day_times, 1)
        };

        if (res.data.status == 103) {
          wx.showModal({
            title: '提示',
            content: '您还未缴纳保证金，请先充值',
            confirmText: '充值',
            confirmColor: '#28B9D1',
            success: function success(res) {
              if (res.confirm) {
                _this.getPay(0.01);
              }
            }
          });
        } else if (res.data.status == 101) {
          var order_sn = res.data.data.order_sn;

          _this.getPay(order_sn, 'door').then(function () {
            _this.$app.$options.globalData.successInfo = info;
            wx.redirectTo({
              url: '/pages/orderSuccess'
            });
          });
        } else {
          _this.$app.$options.globalData.successInfo = info;
          wx.redirectTo({
            url: '/pages/orderSuccess'
          });
        }
      });
    },
    submit: function submit() {
      if (!this.verification()) return;

      var _this = this;

      wx.showModal({
        title: '提示',
        content: '是否确定预约？',
        confirmColor: '#28B9D1',
        success: function success(res) {
          if (res.confirm) {
            _this.identification();
          }
        }
      });
    },
    updateImg: function updateImg() {
      // 上传图片
      var _this = this;

      wx.chooseImage({
        sizeType: ['original'],
        //compressed
        sourceType: ['album', 'camera'],
        success: function success(res) {
          _this.Imglength = res.tempFiles.length;

          for (var i = 0; i < res.tempFiles.length; i++) {
            var element = res.tempFiles[i];

            if (element.size < 10485760) {
              _http["default"].uploadFile(_config["default"].url.upload, element.path).then(function (res) {
                _this.Imglength--;
                var data = JSON.parse(res.data);

                _this.imgList.push(data.data);
              })["catch"](function () {
                _this.Imglength--;
              });
            } else {
              _this.Imglength--;
              wx.showToast({
                title: '图片不能大于10M',
                icon: 'none',
                duration: 2000,
                mask: true
              });
            }
          }
        }
      });
    }
  },
  onLoad: function onLoad() {
    var _this2 = this;

    wx.nextTick(function () {
      _this2.startDay = _this2.getDay();
      _this2.endDay = _this2.getDay(6);
    });
  }
}, {info: {"components":{"van-button":{"path":"./../components/lib/button/index"},"van-cell":{"path":"./../components/lib/cell/index"},"van-cell-group":{"path":"./../components/lib/cell-group/index"},"van-loading":{"path":"./../components/lib/loading/index"},"van-checkbox":{"path":"./../components/lib/checkbox/index"},"cell-input":{"path":"./../components/cell-input"}},"on":{"16-6":["tap"],"16-7":["tap"]}}, handlers: {'16-0': {"tap": function proxy () {
    var $event = arguments[arguments.length - 1];
    var _vm=this;
      return (function () {
        _vm.explain($event);
      })();
    
  }},'16-1': {"tap": function proxy () {
    var $event = arguments[arguments.length - 1];
    var _vm=this;
      return (function () {
        _vm.toTel($event);
      })();
    
  }},'16-2': {"change": function proxy () {
    var $event = arguments[arguments.length - 1];
    var _vm=this;
      return (function () {
        _vm.radioChange($event);
      })();
    
  }},'16-3': {"tap": function proxy (src) {
    
    var _vm=this;
      return (function () {
        _vm.previewImage(src);
      })();
    
  }},'16-4': {"tap": function proxy (index) {
    
    var _vm=this;
      return (function () {
        _vm.delImg(index);
      })();
    
  }},'16-5': {"tap": function proxy () {
    var $event = arguments[arguments.length - 1];
    var _vm=this;
      return (function () {
        _vm.updateImg($event);
      })();
    
  }},'16-6': {"tap": function proxy () {
    var $event = arguments[arguments.length - 1];
    var _vm=this;
      return (function () {
        _vm.boxChange($event);
      })();
    
  }},'16-7': {"tap": function proxy () {
    var $event = arguments[arguments.length - 1];
    var _vm=this;
      return (function () {
        _vm.submit($event);
      })();
    
  }}}, models: {'4': {
      type: "input",
      expr: "phone",
      handler: function set ($v) {
      var _vm=this;
        _vm.phone = $v;
      
    }
    },'5': {
      type: "change",
      expr: "day_time",
      handler: function set ($v) {
      var _vm=this;
        _vm.day_time = $v;
      
    }
    },'6': {
      type: "input",
      expr: "day_time",
      handler: function set ($v) {
      var _vm=this;
        _vm.day_time = $v;
      
    }
    },'7': {
      type: "change",
      expr: "day_times",
      handler: function set ($v) {
      var _vm=this;
        _vm.day_times = $v;
      
    }
    },'8': {
      type: "input",
      expr: "day_times",
      handler: function set ($v) {
      var _vm=this;
        _vm.day_times = $v;
      
    }
    },'9': {
      type: "change",
      expr: "addresVal",
      handler: function set ($v) {
      var _vm=this;
        _vm.addresVal = $v;
      
    }
    },'10': {
      type: "input",
      expr: "addres",
      handler: function set ($v) {
      var _vm=this;
        _vm.addres = $v;
      
    }
    },'11': {
      type: "input",
      expr: "address",
      handler: function set ($v) {
      var _vm=this;
        _vm.address = $v;
      
    }
    },'12': {
      type: "input",
      expr: "name",
      handler: function set ($v) {
      var _vm=this;
        _vm.name = $v;
      
    }
    },'13': {
      type: "change",
      expr: "sex",
      handler: function set ($v) {
      var _vm=this;
        _vm.sex = $v;
      
    }
    },'14': {
      type: "input",
      expr: "age",
      handler: function set ($v) {
      var _vm=this;
        _vm.age = $v;
      
    }
    },'15': {
      type: "input",
      expr: "near",
      handler: function set ($v) {
      var _vm=this;
        _vm.near = $v;
      
    }
    },'16': {
      type: "input",
      expr: "nears",
      handler: function set ($v) {
      var _vm=this;
        _vm.nears = $v;
      
    }
    },'17': {
      type: "input",
      expr: "remark",
      handler: function set ($v) {
      var _vm=this;
        _vm.remark = $v;
      
    }
    }}, refs: undefined });
"use strict";

var _core = _interopRequireDefault(require('./../vendor.js')(0));

var _http = _interopRequireDefault(require('./../service/http.js'));

var _config = _interopRequireDefault(require('./../service/config.js'));

var _store = _interopRequireDefault(require('./../store/index.js'));

var _wxParse = _interopRequireDefault(require('./../components/wxParse/wxParse.js'));

var _x = require('./../vendor.js')(4);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

_core["default"].page({
  store: _store["default"],
  mixins: [],
  data: {
    list: [],
    page: 1,
    limit: 10,
    empty: false,
    NoData: false,
    date: ''
  },
  computed: _objectSpread({}, (0, _x.mapState)(['system_info'])),
  methods: _objectSpread({}, (0, _x.mapMutations)(['getSystemInfo']), {
    homeList: function homeList(type) {
      var _this = this;

      this.empty = false;
      if (this.page == 1 && !type) wx.showLoading({
        title: '加载中'
      });
      return _http["default"].request(_config["default"].url.homeList, {
        page: this.page,
        limit: this.limit
      }).then(function (res) {
        wx.hideLoading();

        if (res.data && res.data.length) {
          if (_this.page > 1) {
            _this.list = _this.list.concat(res.data);
          } else {
            _this.list = res.data;
          }

          for (var i = 0; i < _this.list.length; i++) {
            _wxParse["default"].wxParse('title' + i, 'html', _this.list[i].title, _this.$wx);

            _wxParse["default"].wxParse('resume' + i, 'html', _this.list[i].resume, _this.$wx);

            if (i === _this.list.length - 1) {
              _wxParse["default"].wxParseTemArray("titleListArr", 'title', _this.list.length, _this.$wx);

              _wxParse["default"].wxParseTemArray("resumeListArr", 'resume', _this.list.length, _this.$wx);
            }
          }

          var titleListArr = _this.$wx.data.titleListArr;
          var resumeListArr = _this.$wx.data.resumeListArr;
          titleListArr.forEach(function (item, index) {
            _this.list[index].title = item;
          });
          resumeListArr.forEach(function (item, index) {
            _this.list[index].resume = item;
          });

          if (res.data.length < _this.limit) {
            _this.NoData = false;
          } else {
            _this.NoData = true;
          }
        } else {
          if (_this.page == 1) _this.empty = true;
          _this.NoData = false;
        }

        if (type == 'down') {
          wx.hideNavigationBarLoading(); //完成停止加载

          wx.stopPullDownRefresh(); //停止下拉刷新
        }
      })["catch"](function (err) {
        wx.hideLoading();
        _this.NoData = false;
        if (_this.page == 1) _this.empty = true;

        if (type == 'down') {
          wx.hideNavigationBarLoading(); //完成停止加载

          wx.stopPullDownRefresh(); //停止下拉刷新
        }
      });
    }
  }),
  onReachBottom: function onReachBottom() {
    // 上拉加载
    if (!this.NoData) return;
    this.page = this.page + 1;
    this.homeList();
  },
  onPullDownRefresh: function onPullDownRefresh() {
    // 下拉刷新
    this.page = 1;
    this.empty = false;
    wx.showNavigationBarLoading(); //在标题栏中显示加载

    this.homeList('down');
    this.getSystemInfo();
  },
  onLoad: function onLoad() {
    this.homeList();
  },
  onShow: function onShow() {
    this.getSystemInfo();
  }
}, {info: {"components":{"card-list":{"path":"./../components/card-list"},"card":{"path":"./../components/card"}},"on":{}}, handlers: {}, models: {}, refs: undefined });
"use strict";

var _core = _interopRequireDefault(require('./../vendor.js')(0));

var _http = _interopRequireDefault(require('./../service/http.js'));

var _config = _interopRequireDefault(require('./../service/config.js'));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

_core["default"].page({
  data: {
    empty: false,
    NoData: false,
    page: 1,
    limit: 10,
    list: []
  },
  methods: {
    walletList: function walletList() {
      var _this = this;

      this.empty = false;
      if (this.page == 1) wx.showLoading({
        title: '加载中'
      });
      return _http["default"].request(_config["default"].url.wallet, {
        page: this.page,
        limit: this.limit
      }).then(function (res) {
        wx.hideLoading();

        if (res.data && res.data.length) {
          if (_this.page > 1) {
            _this.list = _this.list.concat(res.data);
          } else {
            _this.list = res.data;
          }

          console.log(res.data.length);
          console.log(_this.limit);

          if (res.data.length < _this.limit) {
            _this.NoData = false;
          } else {
            _this.NoData = true;
          }
        } else {
          if (_this.page == 1) _this.empty = true;
          _this.NoData = false;
        }
      })["catch"](function (err) {
        wx.hideLoading();
        _this.NoData = false;
        if (_this.page == 1) _this.empty = true;
      });
    }
  },
  onReachBottom: function onReachBottom() {
    // 上拉加载
    if (!this.NoData) return;
    this.page = this.page + 1;
    this.walletList();
  },
  onLoad: function onLoad() {
    this.walletList();
  }
}, {info: {"components":{"van-cell":{"path":"./../components/lib/cell/index"},"card-list":{"path":"./../components/card-list"}},"on":{}}, handlers: {}, models: {}, refs: undefined });
"use strict";

var _core = _interopRequireDefault(require('./../vendor.js')(0));

var _x = require('./../vendor.js')(4);

var _store = _interopRequireDefault(require('./../store/index.js'));

var _http = _interopRequireDefault(require('./../service/http.js'));

var _config = _interopRequireDefault(require('./../service/config.js'));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

_core["default"].page({
  store: _store["default"],
  data: {},
  computed: _objectSpread({}, (0, _x.mapState)(['userInfo'])),
  methods: _objectSpread({}, (0, _x.mapActions)(['getInfo']), {
    refund: function refund() {
      var _this2 = this;

      var _this = this;

      wx.showModal({
        title: '提示',
        content: '您确认要退回保证金？',
        confirmColor: '#28B9D1',
        success: function success(res) {
          var info = {
            type: 'wallet'
          };

          if (res.confirm) {
            _http["default"].request(_config["default"].url.order_tx, {
              money: _this2.userInfo.amount
            }).then(function (res) {
              _this.getInfo(); // 更新用户信息内容，主要是余额


              _this.$app.$options.globalData.successInfo = info;
              wx.navigateTo({
                url: '/pages/orderSuccess'
              });
            })["catch"](function (err) {
              wx.showToast({
                title: err,
                icon: 'none',
                mask: true
              });
            });
          }
        }
      });
    }
  }),
  onLoad: function onLoad() {
    this.getInfo();
  }
}, {info: {"components":{"van-cell":{"path":"./../components/lib/cell/index"}},"on":{"21-0":["tap"]}}, handlers: {'21-0': {"tap": function proxy () {
    var $event = arguments[arguments.length - 1];
    var _vm=this;
      return (function () {
        _vm.refund($event);
      })();
    
  }}}, models: {}, refs: undefined });
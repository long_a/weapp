"use strict";

var _core = _interopRequireDefault(require('./../vendor.js')(0));

var _http = _interopRequireDefault(require('./../service/http.js'));

var _config = _interopRequireDefault(require('./../service/config.js'));

var _x = require('./../vendor.js')(4);

var _store = _interopRequireDefault(require('./../store/index.js'));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

_core["default"].page({
  store: _store["default"],
  data: {
    page: 1,
    limit: 10,
    list: [],
    NoData: false,
    empty: false
  },
  computed: _objectSpread({}, (0, _x.mapState)(['system_info'])),
  methods: _objectSpread({}, (0, _x.mapActions)(['getDoor']), {
    doorList: function doorList(type) {
      var _this2 = this;

      this.empty = false;
      if (this.page == 1 && !type) wx.showLoading({
        title: '加载中'
      });

      _http["default"].request(_config["default"].url.doorList, {
        page: this.page,
        limit: this.limit
      }).then(function (res) {
        wx.hideLoading();

        if (res.data && res.data.length) {
          if (_this2.page > 1) {
            _this2.list = _this2.list.concat(res.data);
          } else {
            _this2.list = res.data;
          }

          if (res.data.length < _this2.limit) {
            _this2.NoData = false;
          } else {
            _this2.NoData = true;
          }
        } else {
          if (_this2.page == 1) _this2.empty = true;
          _this2.NoData = false;
        }

        if (type == 'down') {
          wx.hideNavigationBarLoading(); //完成停止加载

          wx.stopPullDownRefresh(); //停止下拉刷新
        }
      })["catch"](function (err) {
        wx.hideLoading();
        if (_this2.page == 1) _this2.empty = true;

        if (type == 'down') {
          wx.hideNavigationBarLoading(); //完成停止加载

          wx.stopPullDownRefresh(); //停止下拉刷新
        }
      });
    },
    getRoute: function getRoute(item) {
      this.getDoor(item);
      wx.navigateTo({
        url: "/pages/doorDetail"
      });
    },
    showModal: function showModal(e) {
      var _this = this;

      wx.showModal({
        title: '提示',
        content: "\u76EE\u524D\u533B\u751F\u4E0A\u95E8\u670D\u52A1\u9700\u901A\u8FC7\u7535\u8BDD".concat(_this.system_info.contact, "\u7535\u8BDD\u9884\u7EA6"),
        confirmText: '拨打',
        confirmColor: '#28B9D1',
        success: function success(res) {
          if (res.confirm) {
            wx.makePhoneCall({
              phoneNumber: _this.system_info.contact
            });
          }
        }
      });
    }
  }),
  onReachBottom: function onReachBottom() {
    // 上拉加载
    if (!this.NoData) return;
    this.page = this.page + 1;
    this.doorList();
  },
  onPullDownRefresh: function onPullDownRefresh() {
    // 下拉刷新
    this.page = 1;
    wx.showNavigationBarLoading(); //在标题栏中显示加载

    this.doorList('down');
  },
  onLoad: function onLoad() {
    console.log(this);
    this.doorList();
  }
}, {info: {"components":{"van-tab":{"path":"./../components/lib/tab/index"},"van-tabs":{"path":"./../components/lib/tabs/index"},"card-list":{"path":"./../components/card-list"}},"on":{"15-0":["disabled"]}}, handlers: {'15-0': {"disabled": function proxy () {
    var $event = arguments[arguments.length - 1];
    var _vm=this;
      return (function () {
        _vm.showModal($event);
      })();
    
  }},'15-1': {"tap": function proxy (item) {
    
    var _vm=this;
      return (function () {
        _vm.getRoute(item);
      })();
    
  }}}, models: {}, refs: undefined });
"use strict";

var _core = _interopRequireDefault(require('./../vendor.js')(0));

var _http = _interopRequireDefault(require('./../service/http.js'));

var _config = _interopRequireDefault(require('./../service/config.js'));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var WxParse = require('./../components/wxParse/wxParse.js');

_core["default"].page({
  data: {
    id: '',
    activeNames: [],
    list: [],
    info: Object,
    morning: Number,
    Afternoon: Number,
    selcetindex: 0,
    ymd: ''
  },
  methods: {
    getOrder: function getOrder(type) {
      if (type === 0 && !this.morning) return;
      if (type === 1 && !this.Afternoon) return;
      this.$app.$options.globalData.commentsDetail = this.info;
      wx.navigateTo({
        url: "/pages/doctorOrder?type=".concat(type, "&index=").concat(this.selcetindex)
      });
    },
    selectDate: function selectDate(item, index) {
      this.morning = item.morning;
      this.Afternoon = item.Afternoon;
      this.selcetindex = index;
    },
    onChange: function onChange(event) {
      this.activeNames = event.$wx.detail[1];
    },
    appointment: function appointment(id) {
      var _this = this;

      wx.showLoading({
        title: '加载中...',
        mask: true
      });

      _http["default"].request(_config["default"].url.appointment, {
        id: id
      }).then(function (res) {
        _this.info = res.data;

        if (_this.info.comments.length) {
          _this.morning = _this.info.comments[0].morning;
          _this.Afternoon = _this.info.comments[0].Afternoon;
          _this.$app.$options.globalData.commentsDetail = _this.info.comments[0];
        }

        WxParse.wxParse('resume', 'html', res.data.resume, _this.$wx);

        _this.list.push(res.data);

        for (var i = 0; i < _this.list.length; i++) {
          WxParse.wxParse('title' + i, 'html', _this.list[i].title, _this.$wx);
          WxParse.wxParse('resume' + i, 'html', _this.list[i].resume, _this.$wx);

          if (i === _this.list.length - 1) {
            WxParse.wxParseTemArray("titleListArr", 'title', _this.list.length, _this.$wx);
            WxParse.wxParseTemArray("resumeListArr", 'resume', _this.list.length, _this.$wx);
          }
        }

        var titleListArr = _this.$wx.data.titleListArr;
        var resumeListArr = _this.$wx.data.resumeListArr;
        titleListArr.forEach(function (item, index) {
          _this.list[index].title = item;
        });
        resumeListArr.forEach(function (item, index) {
          _this.list[index].resume = item;
        });
        wx.hideLoading();
      })["catch"](function (err) {
        wx.hideLoading();
      });
    }
  },
  onLoad: function onLoad(option) {
    this.$app.$options.globalData.info = null;
    this.id = option.id; // this.appointment(option.id)
  },
  onShow: function onShow() {
    if (this.id) {
      this.activeNames = [];
      this.list = [];
      this.info = Object;
      this.morning = Number;
      this.Afternoon = Number;
      this.selcetindex = 0;
      this.ymd = '';
      this.appointment(this.id);
    }
  }
}, {info: {"components":{"van-button":{"path":"./../components/lib/button/index"},"van-cell":{"path":"./../components/lib/cell/index"},"van-collapse":{"path":"./../components/lib/collapse/index"},"van-collapse-item":{"path":"./../components/lib/collapse-item/index"},"van-popup":{"path":"./../components/lib/popup/index"},"card":{"path":"./../components/card"}},"on":{"13-0":["change"],"13-2":["tap"],"13-3":["tap"]}}, handlers: {'13-0': {"change": function proxy () {
    var $event = arguments[arguments.length - 1];
    var _vm=this;
      return (function () {
        _vm.onChange($event);
      })();
    
  }},'13-1': {"tap": function proxy (item, index) {
    
    var _vm=this;
      return (function () {
        _vm.selectDate(item, index);
      })();
    
  }},'13-2': {"tap": function proxy () {
    
    var _vm=this;
      return (function () {
        _vm.getOrder(0);
      })();
    
  }},'13-3': {"tap": function proxy () {
    
    var _vm=this;
      return (function () {
        _vm.getOrder(1);
      })();
    
  }}}, models: {}, refs: undefined });
"use strict";

var _core = _interopRequireDefault(require('./../vendor.js')(0));

var _http = _interopRequireDefault(require('./../service/http.js'));

var _config = _interopRequireDefault(require('./../service/config.js'));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

_core["default"].page({
  data: {
    phone: '',
    // 手机号
    name: '',
    // 姓名
    sendNum: '获取验证码',
    sendTime: null,
    code: '',
    smscode: '' // 验证码

  },
  computed: {
    notNull: function notNull() {
      if (this.phone && this.name && this.smscode) {
        return true;
      } else {
        return false;
      }
    }
  },
  methods: {
    getCode: function getCode(type) {
      var _this2 = this;

      if (!this.phone) {
        wx.showToast({
          title: '手机号不能为空',
          icon: 'none',
          mask: true
        });
        return false;
      }

      if (!/^1[0-9][0-9]{9}$/.test(this.phone)) {
        wx.showToast({
          title: '请输入正确的手机号',
          icon: 'none',
          mask: true
        });
        return false;
      }

      _http["default"].request(_config["default"].url.send_sms, {
        mobile: this.phone
      }).then(function (res) {
        var _this = _this2;
        _this.sendNum = 60;
        _this.code = res.data;
        _this.sendTime = setInterval(function () {
          _this.sendNum--;

          if (_this.sendNum === 0) {
            _this.sendNum = '获取验证码';
            clearInterval(_this.sendTime);
          }
        }, 1000);
      });
    },
    submit: function submit() {
      if (!this.notNull) return;

      if (!/^1[0-9][0-9]{9}$/.test(this.phone)) {
        wx.showToast({
          title: '请输入正确的手机号',
          icon: 'none',
          mask: true
        });
        return false;
      }

      if (this.smscode != this.code) {
        wx.showToast({
          title: '验证码输入不正确',
          icon: 'none',
          mask: true
        });
        return false;
      }

      var params = {
        name: this.name,
        phone: this.phone,
        smscode: this.smscode
      };
      this.$app.$options.globalData.info = params;
      clearInterval(this.sendTime);
      wx.navigateBack(-1);
    }
  },
  onLoad: function onLoad() {
    var info = this.$app.$options.globalData.info;

    if (info) {
      this.name = info.name;
      this.phone = info.phone;
    }
  }
}, {info: {"components":{"van-button":{"path":"./../components/lib/button/index"},"van-cell":{"path":"./../components/lib/cell/index"},"van-cell-group":{"path":"./../components/lib/cell-group/index"}},"on":{"11-1":["tap"]}}, handlers: {'11-0': {"tap": function proxy () {
    var $event = arguments[arguments.length - 1];
    var _vm=this;
      return (function () {
        _vm.getCode($event);
      })();
    
  }},'11-1': {"tap": function proxy () {
    var $event = arguments[arguments.length - 1];
    var _vm=this;
      return (function () {
        _vm.submit($event);
      })();
    
  }}}, models: {'1': {
      type: "input",
      expr: "name",
      handler: function set ($v) {
      var _vm=this;
        _vm.name = $v;
      
    }
    },'2': {
      type: "input",
      expr: "phone",
      handler: function set ($v) {
      var _vm=this;
        _vm.phone = $v;
      
    }
    },'3': {
      type: "input",
      expr: "smscode",
      handler: function set ($v) {
      var _vm=this;
        _vm.smscode = $v;
      
    }
    }}, refs: undefined });
"use strict";

var _core = _interopRequireDefault(require('./../vendor.js')(0));

var _http = _interopRequireDefault(require('./../service/http.js'));

var _config = _interopRequireDefault(require('./../service/config.js'));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

_core["default"].page({
  data: {
    content: ''
  },
  methods: {
    submit: function submit() {
      if (!this.content) return;

      _http["default"].request(_config["default"].url.feedback, {
        content: this.content
      }).then(function (res) {
        console.log(res);
        wx.showToast({
          title: res.data,
          icon: 'success',
          duration: 2000,
          mask: true,
          success: function success() {
            setTimeout(function () {
              wx.navigateBack(-1);
            }, 1500);
          }
        });
      });
    }
  },
  onLoad: function onLoad(option) {}
}, {info: {"components":{"van-button":{"path":"./../components/lib/button/index"}},"on":{"22-0":["tap"]}}, handlers: {'22-0': {"tap": function proxy () {
    var $event = arguments[arguments.length - 1];
    var _vm=this;
      return (function () {
        _vm.submit($event);
      })();
    
  }}}, models: {'0': {
      type: "input",
      expr: "content",
      handler: function set ($v) {
      var _vm=this;
        _vm.content = $v;
      
    }
    }}, refs: undefined });
"use strict";

var _core = _interopRequireDefault(require('./../vendor.js')(0));

var _http = _interopRequireDefault(require('./../service/http.js'));

var _config = _interopRequireDefault(require('./../service/config.js'));

var _wxParse = _interopRequireDefault(require('./../components/wxParse/wxParse.js'));

var _x = require('./../vendor.js')(4);

var _store = _interopRequireDefault(require('./../store/index.js'));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

_core["default"].page({
  store: _store["default"],
  data: {
    page: 1,
    limit: 10,
    tabIndex: 0,
    NoData: false,
    orderEmpty: false,
    doorEmpty: false
  },
  computed: _objectSpread({}, (0, _x.mapState)(['orderList', 'doorList'])),
  methods: _objectSpread({}, (0, _x.mapMutations)(['getDoorList', 'getOrderList']), {
    toPage: function toPage(index) {
      wx.navigateTo({
        url: "/pages/orderDetail?tabIndex=".concat(this.tabIndex, "&index=").concat(index)
      });
    },
    getList: function getList(url, type) {
      var _this = this;

      this.orderEmpty = false;
      this.doorEmpty = false;

      if (this.page == 1) {
        this.NoData = false;
        if (!type) wx.showLoading({
          title: '加载中'
        });
      }

      _http["default"].request(url, {
        page: this.page,
        limit: this.limit
      }).then(function (res) {
        wx.hideLoading();

        if (res.data && res.data.length) {
          var orderList = _this.orderList;
          var doorList = _this.doorList;

          if (_this.tabIndex == 0) {
            for (var i = 0; i < res.data.length; i++) {
              _wxParse["default"].wxParse('title' + i, 'html', res.data[i].group_info.title, _this.$wx);

              if (i === res.data.length - 1) {
                _wxParse["default"].wxParseTemArray("titleListArr", 'title', res.data.length, _this.$wx);
              }
            }

            _this.$wx.data.titleListArr.forEach(function (item, index) {
              res.data[index].group_info.title = item;
            });
          }

          if (_this.page > 1) {
            if (_this.tabIndex == 0) {
              orderList = orderList.concat(res.data);
            } else {
              doorList = doorList.concat(res.data);
            }
          } else {
            _this.tabIndex == 0 ? orderList = res.data : doorList = res.data;
          }

          _this.getDoorList(doorList);

          _this.getOrderList(orderList);

          _this.NoData = res.data.length < _this.limit ? false : true;
        } else {
          if (_this.page == 1) _this.tabIndex == 0 ? _this.orderEmpty = true : _this.doorEmpty = true;
          _this.NoData = false;
        }

        if (type == 'down') {
          wx.hideNavigationBarLoading(); //完成停止加载

          wx.stopPullDownRefresh(); //停止下拉刷新
        }
      })["catch"](function (err) {
        wx.hideLoading();
        if (_this.page == 1) _this.tabIndex == 0 ? _this.orderEmpty = true : _this.doorEmpty = true;

        if (type == 'down') {
          wx.hideNavigationBarLoading(); //完成停止加载

          wx.stopPullDownRefresh(); //停止下拉刷新
        }
      });
    },
    onChange: function onChange(event) {
      this.tabIndex = event.$wx.detail[1].index;
      this.page = 1;

      if (this.tabIndex == 0) {
        this.getList(_config["default"].url.shopDoctorList);
      } else {
        this.getList(_config["default"].url.doorServiceList);
      }
    }
  }),
  onReachBottom: function onReachBottom() {
    // 上拉加载
    if (!this.NoData) return;
    this.page = this.page + 1;

    if (this.tabIndex == 0) {
      this.getList(_config["default"].url.shopDoctorList);
    } else {
      this.getList(_config["default"].url.doorServiceList);
    }
  },
  onPullDownRefresh: function onPullDownRefresh() {
    // 下拉刷新
    this.page = 1;
    wx.showNavigationBarLoading(); //在标题栏中显示加载

    if (this.tabIndex == 0) {
      this.getList(_config["default"].url.shopDoctorList, 'down');
    } else {
      this.getList(_config["default"].url.doorServiceList, 'down');
    }
  },
  onShow: function onShow() {
    var successInfo = this.$app.$options.globalData.successInfo;

    if (successInfo && successInfo.type == 'door') {
      this.tabIndex = 1;
    } else {
      this.tabIndex = 0;
    }

    var url = this.tabIndex ? _config["default"].url.doorServiceList : _config["default"].url.shopDoctorList;
    this.getList(url);
  }
}, {info: {"components":{"van-tab":{"path":"./../components/lib/tab/index"},"van-tabs":{"path":"./../components/lib/tabs/index"},"card-list":{"path":"./../components/card-list"},"order-list":{"path":"./../components/order-list"}},"on":{"18-0":["onChange"],"18-1":["getRoute"],"18-2":["getRoute"]}}, handlers: {'18-0': {"onChange": function proxy () {
    var $event = arguments[arguments.length - 1];
    var _vm=this;
      return (function () {
        _vm.onChange($event);
      })();
    
  }},'18-1': {"getRoute": function proxy () {
    var $event = arguments[arguments.length - 1];
    var _vm=this;
      return (function () {
        _vm.toPage($event);
      })();
    
  }},'18-2': {"getRoute": function proxy () {
    var $event = arguments[arguments.length - 1];
    var _vm=this;
      return (function () {
        _vm.toPage($event);
      })();
    
  }}}, models: {}, refs: undefined });
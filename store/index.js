"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _x = _interopRequireDefault(require('./../vendor.js')(4));

var _http = _interopRequireDefault(require('./../service/http.js'));

var _config = _interopRequireDefault(require('./../service/config.js'));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = new _x["default"].Store({
  state: {
    count: 0,
    userInfo: null,
    orderList: [],
    doorList: [],
    orderDetail: null,
    doorDetail: null,
    system_info: null
  },
  mutations: {
    getUser: function getUser(state, userInfo) {
      state.userInfo = userInfo;
    },
    getInfo: function getInfo(state, open_id) {
      _http["default"].request(_config["default"].url.get_info, {}, 'GET', open_id).then(function (res) {
        state.userInfo = res.data;
      });
    },
    getOrder: function getOrder(state, detail) {
      state.orderDetail = detail;
    },
    getDoor: function getDoor(state, detail) {
      state.doorDetail = detail;
    },
    getOrderList: function getOrderList(state, list) {
      state.orderList = list;
    },
    getDoorList: function getDoorList(state, list) {
      state.doorList = list;
    },
    getSystemInfo: function getSystemInfo(state) {
      _http["default"].request(_config["default"].url.system_info, {}, 'GET').then(function (res) {
        state.system_info = res.data;
      });
    }
  },
  actions: {
    getInfo: function getInfo(_ref, open_id) {
      var commit = _ref.commit;
      commit('getInfo', open_id);
    },
    getOrder: function getOrder(_ref2, detail) {
      var commit = _ref2.commit;
      commit('getOrder', detail);
    },
    getDoor: function getDoor(_ref3, detail) {
      var commit = _ref3.commit;
      commit('getDoor', detail);
    },
    getSystemInfo: function getSystemInfo(_ref4) {
      var commit = _ref4.commit;
      commit('getSystemInfo');
    }
  }
});

exports["default"] = _default;
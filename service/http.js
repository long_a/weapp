import store from '../store/index'
import config from './config'
class Point {
  constructor() {
    
  }
}

class http extends Point {
  constructor() {
    super();
  }
  request(url, params, method = 'POST', open_id) {
    this.userInfo = wx.getStorageSync('user_info')
    if(url !== config.url.homeList){
      if (this.userInfo && this.userInfo.open_id) {
        store.state.userInfo = this.userInfo
      } else if(url !== config.url.system_info) {
        wx.redirectTo({ url: '/pages/login' })
      }
    }
    return new Promise((resolve, reject) => {
      let key = store.state.userInfo ? store.state.userInfo.open_id : ''
      let token = open_id ? open_id : key
      wx.request({
        url: url, //仅为示例，并非真实的接口地址
        data: params,
        method: method,
        header: {
          'content-type': 'application/json', // 默认值
          'Authorization': token
        },
        success: (res) => {
          if (res.statusCode !== 200) {
            wx.showToast({
              title: '系统异常，请稍后重试',
              icon: 'none',
              duration: 2000,
              mask: true
            })
            return reject('系统异常，请稍后重试')
          }
          let data = res.data
          if (data.code == 200) {
            return resolve(res.data)
          } else {
            if (!store.state.userInfo) return reject()
            wx.showToast({
              title: data.message,
              icon: 'none',
              duration: 2000,
              mask: true
            })
            return reject(data.message)
          }
        },
        fail: (err) => {
          if (!store.state.userInfo) return reject()
          wx.showToast({
            title: err || '系统异常，请稍后重试',
            icon: 'none',
            duration: 2000,
            mask: true
          })
          return reject()
        }

      })
    })
  }
  uploadFile(url, filePath) {
    return new Promise((resolve, rejects) => {
      wx.uploadFile({
        url: url,
        filePath: filePath,
        name: 'file',
        header: {
          'Authorization': store.state.userInfo.open_id
        },
        formData: {
          'user': 'test'
        },
        success(res) {
          return resolve(res)
        }
      })
    })
  }
}

export default new http()

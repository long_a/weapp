const testUrl = 'http://zensuo.winderinfo.com' // 测试环境
const devUrl = 'https://zhuangxiu.blihvip.com' // 线上环境
const isdev = 1 //1 线上环境 0 测试环境
const ajaxUrl = isdev ? devUrl : testUrl

export default {
  isdev,
  ajaxUrl,
  url: {
    get_info: `${ajaxUrl}/api/auth/get_info`, // 获取个人信息
    system_info: `${ajaxUrl}/api/index/system_info`, // 获取平台信息
    authorization: `${ajaxUrl}/api/auth/authorization`, // 授权登录
    homeList: `${ajaxUrl}/api/index/index`, //首页
    get_agreement: `${ajaxUrl}/api/auth/get_agreement`, // 用户协议
    doorList: `${ajaxUrl}/api/door/index`, //上门服务列表
    homeIdentification: `${ajaxUrl}/api/index/identification`, // 确认就诊
    shopDoctorList: `${ajaxUrl}/api/index/emplacement_list`, //我的到店就诊
    doorServiceList: `${ajaxUrl}/api/door/emplacement_list`, //上门服务订单
    emplacement_clear: `${ajaxUrl}/api/index/emplacement_clear`, // 取消到店就诊
    identification_clear: `${ajaxUrl}/api/door/identification_clear`, // 取消上门服务
    upload: `${ajaxUrl}/api/auth/upload`, // 上传
    identification: `${ajaxUrl}/api/door/identification`, // 选择上门服务确认
    appointment: `${ajaxUrl}/api/index/make_an_appointment`, // 医生详情
    selection_number: `${ajaxUrl}/api/index/selection_number`, // 医师预约选号
    about_us: `${ajaxUrl}/api/auth/about_us`, // 关于我们
    get_dorr_us: `${ajaxUrl}/api/auth/get_dorr_us`, // 上门服务协议
    feedback: `${ajaxUrl}/api/index/feedback`, // 反馈
    deposit: `${ajaxUrl}/api/auth/deposit`, // 缴纳保证金
    order_tx: `${ajaxUrl}/api/auth/order_tx`, // 体现
    deposit_identification_mz: `${ajaxUrl}/api/index/deposit_identification_mz`, // 医师预约选号确认支付
    deposit_identification: `${ajaxUrl}/api/door/deposit_identification`, //上门服务项目预约支付
    wallet: `${ajaxUrl}/api/index/wallet`, // 我的钱包
    send_sms: `${ajaxUrl}/api/auth/send_sms` // 短信

  }
}
